import { Card } from "./Card";

export class Hand {
  cards: Card[] = [];
  name: string;

  constructor(name: string, arg: string) {
    this.name = name
    const ListCard = arg.split(" ");
    ListCard.forEach((card) => {
      this.cards = [...this.cards, new Card(card)];
    });
  }

  getArrayValueCard(): number[] {
    return   [
      this.getValueCardOne(),
      this.getValueCardTwo(),
      this.getValueCardThree(),
      this.getValueCardFour(),
      this.getValueCardFive(),
    ];
  }

  hasRepetitionCard(): number[] {
    return this.getArrayValueCard().filter ( (v,i,a) => a.indexOf(v) < i );
  }

  isPair():boolean {
    return this.hasRepetitionCard().length === 1
  }

  istriplet():boolean {
    return this.hasRepetitionCard().length === 2
  }
  hasPoker():boolean {
    return this.hasRepetitionCard().length === 3
  }

  isColor(): boolean {
    return (
      this.getPaloCardOne() === this.getPaloCardTwo() &&
      this.getPaloCardTwo() === this.getPaloCardThree() &&
      this.getPaloCardThree() === this.getPaloCardFour() &&
      this.getPaloCardFour() === this.getPaloCardFive()
    );
  }

  hasColorAndLadder(): boolean {
    return this.isColor() && this.isLadder()
  }

  highLetter(): number { 
    return Math.max(...this.getArrayValueCard())
  }

  isLadder(): boolean {

    let invalid = false;
    for (var i = 0; i < this.getArrayValueCard().length - 1; i++) {
      if (Math.abs(this.getArrayValueCard()[i] - this.getArrayValueCard()[i + 1]) > 1) {
        invalid = true;
        break;
      }
    }
    return !invalid;
  }

  getPaloCardOne(): string {
    return this.cards[0].palo;
  }
  getPaloCardTwo(): string {
    return this.cards[1].palo;
  }
  getPaloCardThree(): string {
    return this.cards[2].palo;
  }
  getPaloCardFour(): string {
    return this.cards[3].palo;
  }
  getPaloCardFive(): string {
    return this.cards[4].palo;
  }

  getValueCardOne(): number {
    return this.cards[0].value;
  }
  getValueCardTwo(): number {
    return this.cards[1].value;
  }
  getValueCardThree(): number {
    return this.cards[2].value;
  }
  getValueCardFour(): number {
    return this.cards[3].value;
  }
  getValueCardFive(): number {
    return this.cards[4].value;
  }
}
