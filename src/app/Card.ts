export class Card {
  value: number = 0;
  palo: string = "";
  constructor(arg: string) {
    this.DetectValue(arg);
  }

  DetectValue(arg: string): void {
    if (arg.length == 2) {
      const value = arg[0];
      if (value === "A") {
        this.value = 14;
        this.palo = arg[1];
      } else if (value === "K") {
        this.value = 13;
        this.palo = arg[1];
      } else if (value === "Q") {
        this.value = 12;
        this.palo = arg[1];
      } else if (value === "J") {
        this.value = 11;
        this.palo = arg[1];
      } else {
        this.value = Number(value);
        this.palo = arg[1];
      }
    }

    if (arg.length == 3) {
      const data = arg[0] + arg[1];
      this.value = Number(data);
      this.palo = arg[2];
    }
  }
}
