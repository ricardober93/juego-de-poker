import { Hand } from "./Hand";

export class Poker {
  constructor() {}

  play(hand: Hand, hand2: Hand): string {
   return  this.whoWinner(hand, hand2)!;
  }

  whoWinner(hand: Hand, hand2: Hand): string | undefined {
    //   Escalera y color
    if (hand.hasColorAndLadder() && !hand2.hasColorAndLadder()) {
      return "Mano 1 gano con Escalera de color";
    }
    if (!hand.hasColorAndLadder() && hand2.hasColorAndLadder()) {
      return "Mano 2 gano con Escalera de color";
    }
    //  POKER
    if (hand.hasPoker() && !hand2.hasPoker()) {
      return "Mano 1 gano con poker";
    }
    if (!hand.hasPoker() && hand2.hasPoker()) {
      return "Mano 2 gano con poker";
    }

    //  Color
    if (hand.isColor() && !hand2.isColor()) {
      return "Mano 1 gano con color";
    }
    if (!hand.isColor() && hand2.isColor()) {
      return "Mano 2 gano con color";
    }

    //  Escalera
    if (hand.isLadder() && !hand2.isLadder()) {
      return "Mano 1 gano con escalera";
    }
    if (!hand.isLadder() && hand2.isLadder()) {
      return "Mano 2 gano con escalera";
    }

    //  trio
    if (hand.istriplet() && !hand2.istriplet()) {
      return "Mano 1 gano con tripleta";
    }
    if (!hand.istriplet() && hand2.istriplet()) {
      return "Mano 2 gano con tripleta";
    }

    //  par
    if (hand.isPair() && !hand2.isPair()) {
      return "Mano 1 gano con par";
    }
    if (!hand.isPair() && hand2.isPair()) {
      return "Mano 2 gano con par";
    }

    //  Carta Alta
    if (hand.highLetter() > hand2.highLetter()) {
      return "Mano 1 gano con Carta Alta";
    }
    if (hand.highLetter() < hand2.highLetter()) {
      return "Mano 2 gano con Carta Alta";
    }
  }
}
