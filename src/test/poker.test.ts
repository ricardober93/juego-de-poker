import { Hand } from "../app/Hand";
import { Poker } from "../app/Poker";

describe("Verificaciones de ganadores", () => {
  it("should Mano 1 Win with Escalera de color", () => {
    const hand1 = new Hand(  'blanco', "7D 8D 9D 10D JD");
    const hand2 = new Hand('negro',"7D AW 3D 5W 6S");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 1 gano con Escalera de color");
  });

  it("should Mano 2 Win with  Escalera de color", () => {
    const hand1 = new Hand( 'blanco',"7D 8W 9D 10D JD");
    const hand2 = new Hand( 'negro',"3W 4W 5W 6W 7W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 2 gano con Escalera de color");
  });

  //   poker
  it("should Mano 1 Win with poker", () => {
    const hand1 = new Hand( 'blanco',"7D 7S 7W 7D JD");
    const hand2 = new Hand( 'negro',"3W 6D 6W 6S 10W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 1 gano con poker");
  });

  it("should Mano 2 Win with  poker", () => {
    const hand1 = new Hand( 'blanco',"7D 5W 3D 10D AD");
    const hand2 = new Hand( 'negro',"2W 2W 2S 2D 9W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 2 gano con poker");
  });

  //   Escalera

  it("should Mano 1 Win with Ladder", () => {
    const hand1 = new Hand( 'blanco',"7D 8S 9W 10D JD");
    const hand2 = new Hand( 'negro',"3W 6D 6W 6S 10W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 1 gano con escalera");
  });

  it("should Mano 2 Win with Ladder", () => {
    const hand1 = new Hand( 'blanco',"7D 5W 3D 10D AD");
    const hand2 = new Hand( 'negro',"2W 3W 4S 5D 6W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 2 gano con escalera");
  });

   //   Color

   it("should Mano 1 Win with Color", () => {
    const hand1 = new Hand( 'blanco',"7S 2S 3S 10S JS");
    const hand2 = new Hand( 'negro',"3W 6D 6W 6S 10W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 1 gano con color");
  });

  it("should Mano 2 Win with Color", () => {
    const hand1 = new Hand( 'blanco',"7D 5W 3D 10D AD");
    const hand2 = new Hand( 'negro',"2W 3W 4W AW 6W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 2 gano con color");
  });


   //   CARTA ALTA

   it("should Mano 1 Win with High Letter", () => {
    const hand1 = new Hand( 'blanco',"7S 2S 3D 10W JS");
    const hand2 = new Hand( 'negro',"3W 4D 2W 5S 9W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 1 gano con Carta Alta");
  });

  it("should Mano 2 Win with High Letter", () => {
    const hand1 = new Hand( 'blanco',"2D 4W 6D 7D 5D");
    const hand2 = new Hand( 'negro',"5W 3S 4S 2W 8W");

    const winner = new Poker().play(hand1, hand2);
    expect(winner).toBe("Mano 2 gano con Carta Alta");
  });
});
